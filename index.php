<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
    require "animal.php";
    $sheep = new Animal("shaun");
    

     echo $sheep->name; // "shaun"
     echo "<br>";
     echo $sheep->legs; // 2
     echo "<br>";
     echo $sheep->cold_blooded; // false
     
     echo "<br><br>";
     // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
     
     
     // index.php
     require "ape.php";
     $sungokong = new Ape("kera sakti");
     $sungokong->yell(); // "Auooo"
     
     echo "<br><br>";
     require "frog.php";
     $kodok = new Frog("buduk");
     $kodok->jump() ; // "hop hop"
    
    
    
    
    
    ?>
</body>
</html>